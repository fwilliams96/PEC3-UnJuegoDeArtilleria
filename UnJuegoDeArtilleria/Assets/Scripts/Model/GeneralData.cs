using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GeneralData : MonoBehaviour
{
    public static GeneralData instance { get; private set; }

    [Header("Game prefabs")]
    public GameObject fireballPrefab;

    public GameObject goombaPrefab;
    
    private void Awake()
    {
        if (instance == null) {
            DontDestroyOnLoad(gameObject);
            instance = this;
        }
    }


}
