using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BasicBoxManager : MonoBehaviour
{
    [SerializeField] protected Animator m_Animator;
    protected bool canInteract = true;
    
    /// <summary>
    /// We only detect a collision if it's the Player and then we activate the shake movement of the box. In addition, we call the virtual function in case a subclass wants to override it
    /// </summary>
    /// <param name="other"></param>
    private void OnCollisionEnter2D(Collision2D other)
    {
        if (canInteract && other.gameObject.CompareTag("Player"))
        {
            m_Animator.SetTrigger("Shake");
            AddExtraFunctionalityOnCollide();
        }
    }

    /// <summary>
    /// Play the shake sound invoked from the shake animation of the box
    /// </summary>
    public void PlayShakeSound()
    {
        Debug.Log("Play shake sound");
        AudioManager.instance.Play("ShakeBox");
    }

    protected virtual void AddExtraFunctionalityOnCollide() {}
    
}
