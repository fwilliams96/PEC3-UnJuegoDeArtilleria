using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Random = UnityEngine.Random;

public class EnemyRespawner : MonoBehaviour
{
    [SerializeField] private List<Transform> respawnPositions;
    [SerializeField] private float timeTillNextRespawn = 3;
    private float currentTime = 0;
    [SerializeField] private bool counting = false;
    
    private void Start()
    {
        counting = true;
        currentTime = timeTillNextRespawn;
    }

    /// <summary>
    /// Every 2 seconds a new goomba will respawn
    /// </summary>
    private void Update()
    {
        if (counting)
        {
            currentTime -= Time.deltaTime;
            if ( currentTime <= 0)
            {
                RespawnGoomba();
                currentTime = timeTillNextRespawn;
            }
        }
    }

    /// <summary>
    /// Instantiate Goombas in a random place from the level
    /// </summary>
    private void RespawnGoomba()
    {
        int randomRespawnPoint = Random.Range(0, respawnPositions.Count);
        Instantiate(GeneralData.instance.goombaPrefab, respawnPositions[randomRespawnPoint].position, Quaternion.identity);
    }

    /// <summary>
    /// Stop respawning enemies when the game is over
    /// </summary>
    public void Stop()
    {
        counting = false;
    }
    
}
