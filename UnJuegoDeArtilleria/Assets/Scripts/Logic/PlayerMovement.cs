using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMovement : MonoBehaviour
{

    [SerializeField] private float runSpeed = 100f;
    [SerializeField] private float jumpForce = 2f;
    [SerializeField] private float checkRadius = 0.5f;
    
    [SerializeField] private Rigidbody2D m_Rigidbody2D;
    [SerializeField] private Transform groundCheck;
    [SerializeField] private PlayerInput playerInput;
    [SerializeField] private PlayerAnimation playerAnimation;
    [SerializeField] private PlayerManager playerManager;
    
    private bool isGrounded = true;


    /// <summary>
    /// We only want to move the player while we are playing
    /// </summary>
    private void FixedUpdate()
    {
        if (GameManager.instance.gameStatus == GameStatus.Playing)
        {
            CheckGroundStatus();
            DeterminePlayerMovement();
            FlipPlayer();
        }
    }

    /// <summary>
    /// We check if we are grounded using a circle raycast and filtering by 'Platforms' layer mask
    /// </summary>
    private void CheckGroundStatus()
    {
        var finalMask = (1 << LayerMask.NameToLayer("Platforms")) | (1 << LayerMask.NameToLayer("Items"));
        isGrounded = Physics2D.OverlapCircle(groundCheck.position, checkRadius, finalMask);
    }

    /// <summary>
    /// We draw a gizmos of the circle that is checking the ground
    /// </summary>
    private void OnDrawGizmos()
    {
        Gizmos.color = Color.yellow;
        Gizmos.DrawSphere(groundCheck.position, checkRadius);
    }

    /// <summary>
    /// Depending on the velocity we identify if the player is moving to the right or to the left, and we flip the sprite rendered
    /// </summary>
    private void FlipPlayer()
    {
        if (playerInput.horizontal < 0)
        {
            playerAnimation.FlipPlayerToLeft();
        }
        else if(playerInput.horizontal > 0)
        {
            playerAnimation.FlipPlayerToRight();
        }
    }

    /// <summary>
    /// We move the playr using the horizontal output variable of the PlayerInput class and we add a vertical force in case the player wants to jump.
    /// We also update the animations here using the PlayerAnimation class
    /// </summary>
    private void DeterminePlayerMovement()
    {
        // Update always the velocity to move the player and to stop him
        m_Rigidbody2D.velocity = new Vector2(playerInput.horizontal * runSpeed * Time.fixedDeltaTime, m_Rigidbody2D.velocity.y);
        
        if (playerInput.jumpPressed && isGrounded)
        {
            // Add a force in the Y axis to jump
            Debug.Log("Player jumps");
            m_Rigidbody2D.AddForce(Vector2.up * jumpForce, ForceMode2D.Impulse);
            playerAnimation.PlayJumpAnimation();
        }
        else
        {
            playerAnimation.StopJumpAnimation();
        }

        if (playerInput.shot && playerManager.CanShot())
        {
            playerAnimation.ThrowBall();
        }
        
        if (playerInput.horizontal != 0 && isGrounded)
        {
            playerAnimation.PlayRunningAnimation();
        }
        else
        {
            playerAnimation.StopRunningAnimation();
        }
        
    }

    /// <summary>
    /// We set the position of the player according to the closest respawn point
    /// </summary>
    /// <param name="closestRespawnPoint"></param>
    public void RespawnPlayer(Vector3 closestRespawnPoint)
    {
        Debug.Log("Respawn player");
        transform.position = closestRespawnPoint;
        Physics.SyncTransforms();
    }
}
