using System;
using System.Collections;
using System.Collections.Generic;
using Cinemachine;
using UnityEngine;
using UnityEngine.SceneManagement;

public enum GameStatus
{
    MainMenu,
    StartGame,
    Playing,
    GameOver,
    Victory,
    EndGame
}
public class GameManager : MonoBehaviour
{
    [SerializeField] private Canvas mainMenuCanvas;
    [SerializeField] private Canvas gameCanvas;
    [SerializeField] private Canvas gameOverCanvas;
    [SerializeField] private Canvas victoryCanvas;
    [SerializeField] private GameTimer gameTimer;
    [SerializeField] private EnemyRespawner enemyRespawner;
    
    public static GameManager instance = null;
    public GameStatus gameStatus;
    private CinemachineVirtualCamera cmPlayerCam;

    private void Awake()
    {
        instance = this;
    }

    private void Start()
    {
        gameStatus = GameStatus.MainMenu;
        GameObject playerCamGo = GameObject.FindWithTag("CMPlayer");
        if (playerCamGo != null)
        {
            cmPlayerCam = playerCamGo.GetComponent<CinemachineVirtualCamera>();
        }
        
    }

    private void Update()
    {
        if (gameStatus == GameStatus.MainMenu)
        {
            ManageMainMenu();
        }
        else if (gameStatus == GameStatus.StartGame)
        {
            ManageStartGame();
        } 
        else if (gameStatus == GameStatus.GameOver)
        {
            ManageGameOver();
        }
        else if (gameStatus == GameStatus.Victory)
        {
            ManageVictory();
        }
    }

    /// <summary>
    /// When the main menu appears, this function is waiting for us to press any button and start the game
    /// </summary>
    private void ManageMainMenu()
    {
        if (Input.anyKey)
        {
            gameStatus = GameStatus.StartGame;
        }
    }

    /// <summary>
    /// When the game starts, we disable the main menu canvas, we enable the game canvas and we start counting
    /// </summary>
    private void ManageStartGame()
    {
        mainMenuCanvas.enabled = false;
        gameCanvas.enabled = true;
        gameTimer.StartCounting();
        gameStatus = GameStatus.Playing;
    }
    
    /// <summary>
    /// When the player dies, we stop the main theme, activate the game over sound, we stop the timer and finally we show the game over canvas
    /// </summary>
    private void ManageGameOver()
    {
        AudioManager.instance.StopMainTheme();
        AudioManager.instance.Play("MarioDies");
        gameOverCanvas.enabled = true;
        gameTimer.StopCounting();
        enemyRespawner.Stop();
        gameStatus = GameStatus.EndGame;
        StopFollowingPlayer();
    }

    /// <summary>
    /// When the player wins, we stop the main theme, activate the victory sound, we stop the timer and finally we show the victory canvas
    /// </summary>
    private void ManageVictory()
    {
        AudioManager.instance.StopMainTheme();
        AudioManager.instance.Play("MarioWins");
        victoryCanvas.enabled = true;
        gameTimer.StopCounting();
        enemyRespawner.Stop();
        gameStatus = GameStatus.EndGame;
    }

    
    /// <summary>
    /// This function is called from the buttons of the game over and victory canvas, we basically reload the scene to start a new game
    /// </summary>
    public void RestartGame()
    {
        var currentScene = SceneManager.GetActiveScene(); 
        SceneManager.LoadScene(currentScene.name);
    }

    /// <summary>
    /// This function is called from the PlayerManager to tell the system we have lost
    /// </summary>
    public void GameOver()
    {
        gameStatus = GameStatus.GameOver;
    }

    /// <summary>
    /// This function is called from the PlayerManager to tell the system we have won
    /// </summary>
    public void Victory()
    {
        gameStatus = GameStatus.Victory;
    }

    /// <summary>
    /// It allows 
    /// </summary>
    private void StopFollowingPlayer()
    {
        if (cmPlayerCam != null)
        {
            cmPlayerCam.Follow = null;
        }
    }
}
