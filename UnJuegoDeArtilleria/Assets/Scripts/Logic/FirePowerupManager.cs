using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FirePowerupManager : PowerupManager
{
    private Vector2 horizontalDirection;
    [SerializeField] private Transform frontCheck;
    [SerializeField] private Transform groundCheck;
    [SerializeField] private float horizontalCheckRadius = 0.5f;
    [SerializeField] private float groundCheckRadius = 0.5f;
    [SerializeField] private float speed = 2f;
    private bool isGrounded;
    
    /// <summary>
    /// We draw a gizmos of the circle that is checking the front of the powerup and the ground check
    /// </summary>
    private void OnDrawGizmos()
    {
        Gizmos.color = Color.yellow;
        Gizmos.DrawSphere(frontCheck.position, horizontalCheckRadius);
        Gizmos.DrawSphere(groundCheck.position, groundCheckRadius);
    }
    
    /// <summary>
    /// The powerup will go to the right by default
    /// </summary>
    protected override void InitializePowerup()
    {
        horizontalDirection = Vector2.right;
    }
    
    /// <summary>
    /// We play a sound when the power up (mushroom) appears
    /// </summary>
    protected override void OnPowerupAppears()
    {
        AudioManager.instance.Play("PowerupAppears");
    }

    /// <summary>
    /// The horizontal check will detect if the mushroom or powerup will collide with some item or brick, in this case will flip horizontally
    /// The powerup will only move if it is on the ground
    /// </summary>
    protected override void UpdatePowerup()
    {
        var finalMaskGroundCheck = (1 << LayerMask.NameToLayer("Platforms")) | (1 << LayerMask.NameToLayer("Items"));

        var horizontalHit = Physics2D.OverlapCircle(frontCheck.position, horizontalCheckRadius, 1 << LayerMask.NameToLayer("Items"));
        isGrounded = Physics2D.OverlapCircle(groundCheck.position, groundCheckRadius, finalMaskGroundCheck);

        if (horizontalHit)
        {
            Debug.Log("Powerup collision with item");

            // Rotate front check
            var frontCheckLocalPosition = frontCheck.localPosition;
            frontCheckLocalPosition.x = frontCheckLocalPosition.x * (-1);
            frontCheck.position = transform.TransformPoint(frontCheckLocalPosition);
            
            horizontalDirection = horizontalDirection * (-1);
        }
    }

    /// <summary>
    /// We will move the powerup only in the fixed update with a specific speed easily modifiable
    /// </summary>
    protected override void FixedUpdatePowerup()
    {
        if (isGrounded)
        {
            transform.Translate(horizontalDirection * Time.deltaTime * speed);
        }
    }

    /// <summary>
    /// When the player collides with the powerup we will call the CollectPower function of the player immediately
    /// Then the powerup will be destroyed
    /// </summary>
    /// <param name="player"></param>
    protected override void PickUpPower(GameObject player)
    {
        player.GetComponent<PlayerManager>().CollectPower(PowerMode.Fire);
        Destroy(gameObject);
    }
}
