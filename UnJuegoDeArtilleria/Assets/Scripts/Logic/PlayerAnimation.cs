using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerAnimation : MonoBehaviour
{
    [SerializeField] private ParticleSystem _dust;
    [SerializeField] private Animator _animator;
    [SerializeField] private SpriteRenderer m_SpriteRenderer;
    [SerializeField] private Transform ballRespawn;

    private Color initialSpriteColor;

    private void Start()
    {
        initialSpriteColor = m_SpriteRenderer.material.color;
    }

    public void PlayRunningAnimation()
    {
        _animator.SetBool("IsRunning", true);
    }

    public void StopRunningAnimation()
    {
        _animator.SetBool("IsRunning", false);
    }
    
    public void PlayJumpAnimation()
    {
        _animator.SetTrigger("Jump");
        PlayDust();
    }

    public void StopJumpAnimation()
    {
        _animator.ResetTrigger("Jump");
    }

    public void PlayDeathAnimation()
    {
        _animator.SetTrigger("Death");   
    }

    public void PlayNormalToFireTransitionAnimation()
    {
        _animator.SetLayerWeight(_animator.GetLayerIndex("BaseLayer"), 0f);
        _animator.SetLayerWeight(_animator.GetLayerIndex("FireLayer"), 1f);
        _animator.SetTrigger("Power");
    }
    
    /// <summary>
    /// We wait some time before transitioning to the base layer to play the fire to normal animation
    /// </summary>
    public void PlayFireToNormalTransitionAnimation()
    {
        Debug.Log("PlayFireToNormalTransitionAnimation");
        _animator.SetTrigger("Normal");
        StartCoroutine(ChangeToBaseLayer());
    }

    public void PlayShotAnimation()
    {
        _animator.SetTrigger("Shot");
    }

    /// <summary>
    /// Let's wait some time to finish the fire to normal animation
    /// Once it's finished, then we can remove the fire layer
    /// </summary>
    /// <returns></returns>
    IEnumerator ChangeToBaseLayer()
    {
        yield return new WaitForSeconds(0.8f);
        _animator.SetLayerWeight(_animator.GetLayerIndex("BaseLayer"), 1f);
        _animator.SetLayerWeight(_animator.GetLayerIndex("FireLayer"), 0f);
    }
    
    /// <summary>
    /// We execute the BlinkSpriteToRed function as a coroutine
    /// Then the mario injured sound is played
    /// </summary>
    public void PlayInjuredAnimation()
    {
       StartCoroutine(BlinkSpriteToRed());
       AudioManager.instance.Play("MarioInjured");
    }
    
    /// <summary>
    /// We blink the player from the current sprite color to a red color, as many times as the value of n
    /// </summary>
    /// <returns></returns>
    IEnumerator BlinkSpriteToRed()
    {
        for (var n = 0; n < 4; n++)
        {
            m_SpriteRenderer.material.color = new Color(1, 0, 0, initialSpriteColor.a);
            yield return new WaitForSeconds(0.2f);
            m_SpriteRenderer.material.color = initialSpriteColor;
            yield return new WaitForSeconds(0.2f);
        }
    }
    
    /// <summary>
    /// Function called from an animation event in the jump animation
    /// </summary>
    public void PlayJumpSound()
    {
        Debug.Log("PlayJumpSound");
        AudioManager.instance.Play("MarioJump");
    }

    public void FlipPlayerToLeft()
    {
        if (!m_SpriteRenderer.flipX)
        {
            m_SpriteRenderer.flipX = true;
            PlayDust();
        }
    }
    
    public void FlipPlayerToRight()
    {
        if (m_SpriteRenderer.flipX)
        {
            m_SpriteRenderer.flipX = false;
            PlayDust();
        }
    }

    private void PlayDust()
    {
        if (_dust != null)
        {
            _dust.Play();    
        }
    }

    private void StopDust()
    {
        if (_dust != null)
        {
            _dust.Stop();    
        }
    }

    /// <summary>
    /// This function is called from the player movement script when this player can throw a ball (fire state)
    /// Depending on the flipX check of the sprite we will determine the horizontal direction of the fireball
    /// We also play the animation shot and finally we will execute the fireball sound
    /// </summary>
    public void ThrowBall()
    {
        Debug.Log("Player shots");
        PlayShotAnimation();
        var ballRespawnLocalPosition = ballRespawn.localPosition;
        var rightDirection = !m_SpriteRenderer.flipX;
        if (!rightDirection)
        {
            ballRespawnLocalPosition.x = ballRespawnLocalPosition.x * (-1);
        }
        
        var ball = Instantiate(GeneralData.instance.fireballPrefab, transform.TransformPoint(ballRespawnLocalPosition), transform.rotation);
        ball.GetComponent<FireballManager>().ThrowBall(rightDirection);
        ball.GetComponent<FireballManager>().player = gameObject.transform.parent.gameObject;
        AudioManager.instance.Play("MarioShot");
    }
    
}