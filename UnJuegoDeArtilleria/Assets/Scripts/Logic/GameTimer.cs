using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameTimer : MonoBehaviour
{
    [SerializeField] private PlayerHUD playerHud;
    [SerializeField] private PlayerManager playerManager;
    [SerializeField] private float totalTime = 300;

    private bool counting = false;
    
    /// <summary>
    /// We count only if the counting bool is active. When the totalTime variable reaches 0 or less we automatically call to the Kill() function of the player
    /// </summary>
    private void Update()
    {
        if (counting)
        {
            totalTime -= Time.deltaTime;
            playerHud.UpdateTime(totalTime);
            if ( totalTime <= 0)
            {
                counting = false;
                playerManager.Kill();
            }
        }

    }

    /// <summary>
    /// Function called from the GameManager to start counting
    /// </summary>
    public void StartCounting()
    {
        counting = true;
    }

    /// <summary>
    /// Function called from the GameManager to stop counting
    /// </summary>
    public void StopCounting()
    {
        counting = false;
    }
}
