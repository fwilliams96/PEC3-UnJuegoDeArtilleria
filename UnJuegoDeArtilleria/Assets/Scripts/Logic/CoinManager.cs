using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CoinManager : MonoBehaviour
{
    /// <summary>
    /// We only want to collide with the player and then we tell the player to collect a coin
    /// We also reproduce the coin sound and finally the object is destroyed
    /// </summary>
    /// <param name="other"></param>
    private void OnTriggerEnter2D(Collider2D other)
    {
        if (other.gameObject.CompareTag("Player"))
        {
            other.gameObject.GetComponent<PlayerManager>().CollectCoin();
            AudioManager.instance.Play("CollectCoin");
            Destroy(gameObject);
        }
    }
    
}
