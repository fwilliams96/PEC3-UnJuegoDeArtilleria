using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using UnityEngine;
using UnityEngine.UI;
using Debug = UnityEngine.Debug;

public class EnemyManager : MonoBehaviour
{
    [SerializeField] private Transform deathPoint;
    [SerializeField] private Collider2D m_Collider2D;
    [SerializeField] private DamageEffect damageEffect;

    [SerializeField] private EnemyAnimation enemyAnimation;

    [HideInInspector] public bool isDead = false;

    /// <summary>
    /// If the player hits the enemy above a certain height, then the Goomba has to die, otherwise the player will lose.
    /// When the enemy is killed, we show the damage text, we play the death animation and finally we disable the collider to allow the player fall properly to the platform.
    /// We set the variable isDead to true to also stop the WaypointPatrol movement
    /// After a few milliseconds we destroy the object
    /// </summary>
    /// <param name="other"></param>
    private void OnCollisionEnter2D(Collision2D other)
    {
        if (!isDead && other.gameObject.CompareTag("Player"))
        {
            Debug.Log("Enemy collision");
            //Player is on top of the enemy
            if (PlayerIsOnTopOfEnemy(other))
            {
                Kill(other.gameObject);
            }
            // Player is below the head of the enemy
            else
            {
                other.gameObject.GetComponent<PlayerManager>().ReduceLife();
            }
        }
    }

    /// <summary>
    /// Kill enemy, this can be called from the player jumping above or from a fireball impact
    /// </summary>
    /// <param name="player"></param>
    public void Kill(GameObject player)
    {
        damageEffect.DisplayDamageEffect(50);
        player.GetComponent<PlayerManager>().AddPoints(50);
        Debug.Log("Enemy dies");
        enemyAnimation.PlayDeathAnimation();
        m_Collider2D.enabled = false;
        isDead = true;
        Destroy(gameObject, 0.5f);
    }

    /// <summary>
    /// This sound is called from the death animation of the Goomba
    /// </summary>
    public void PlayGoombaDiesSound()
    {
        AudioManager.instance.Play("GoombaDies");
    }

    /// <summary>
    /// We check if the world position of the player is higher than the minimum height required to kill the enemy
    /// </summary>
    /// <param name="player"></param>
    /// <returns></returns>
    private bool PlayerIsOnTopOfEnemy(Collision2D player)
    {
        return player.transform.position.y >= deathPoint.position.y;
    }
}
