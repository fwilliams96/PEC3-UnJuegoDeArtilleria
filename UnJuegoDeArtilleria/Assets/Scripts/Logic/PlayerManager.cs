using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public enum PowerMode
{
    NoPower,
    Fire
}

public class PlayerManager : MonoBehaviour
{

    [SerializeField] Rigidbody2D m_Rigibody2D;
    [SerializeField] Collider2D m_LittleCollider2D;
    [SerializeField] Collider2D m_BigCollider2D;
    [SerializeField] private PlayerHUD playerHud;
    [SerializeField] private PlayerAnimation playerAnimation;
    [SerializeField] private PlayerMovement playerMovement;

    private bool untouchable = false;
    private PowerMode powerMode;
    
    private void Start()
    {
        powerMode = PowerMode.NoPower;
    }

    public void DisableColliders()
    {
        if (m_LittleCollider2D != null)
        {
            m_LittleCollider2D.enabled = false;    
        }
        if (m_BigCollider2D != null)
        {
            m_BigCollider2D.enabled = false;    
        }
    }

    /// <summary>
    /// When the player wins we force the player to stop immediately by setting the rigidbody to kinematic and also setting all the velocities to 0
    /// Finally we notify the player hud to update the points and we also tell the GameManager the player has won
    /// </summary>
    public void Win()
    {
        Debug.Log("Player wins");
        m_Rigibody2D.isKinematic = true;
        m_Rigibody2D.velocity=new Vector2(0,0);
        m_Rigibody2D.angularVelocity = 0;
        playerHud.AddPointsOneByOne(100);
        GameManager.instance.Victory();
    }

    /// <summary>
    /// This function is called from the EnemyManager when we kill a Goomba
    /// </summary>
    /// <param name="points"></param>
    public void AddPoints(int points)
    {
        playerHud.AddPointsOneByOne(points);
    }

    /// <summary>
    /// This function is called from the CoinManager to collect a coin
    /// </summary>
    public void CollectCoin()
    {
        playerHud.CollectCoin();
    }

    /// <summary>
    /// This function is called when a power is collected, if Mario already has the power then we will ignore the change
    /// We enable the big collider and we disable the little one
    /// </summary>
    public void CollectPower(PowerMode powerMode)
    {
        if (this.powerMode == powerMode)
        {
            AudioManager.instance.Play("CollectPowerup");
            return;
        }
        
        this.powerMode = powerMode;
        if (PowerMode.Fire == powerMode)
        {
            playerAnimation.PlayNormalToFireTransitionAnimation();
            m_LittleCollider2D.enabled = false;
            m_BigCollider2D.enabled = true;
            AudioManager.instance.Play("CollectPowerup");
        }
    }

    
    /// <summary>
    /// We play the death animation, we disable the collider to let the player drop and we finally notify the GameManager the player is dead
    /// We update the status of the isDead bool to avoid the multiple collision problem 
    /// </summary>
    public void Kill()
    {
        Debug.Log("Player dies");
        if (untouchable) return;

        playerAnimation.PlayDeathAnimation();
        Invoke(nameof(DisableColliders), 1f);
        GameManager.instance.GameOver();
        untouchable = true;

    }

    /// <summary>
    /// When this function is called we will reduce one life of our player, if the player goes down to 0 then we will kill him
    /// We update the status of the untouchable bool to avoid the multiple collision problem 
    /// </summary>
    public void ReduceLife(bool fallen = false)
    {
        if (untouchable) return;
        if (fallen)
        {
            Debug.Log("Player has fallen");
            playerHud.ReduceLife();
            if (playerHud.Lives <= 0)
            {
                Debug.Log("Kill player");
                Kill();
                return;
            }
            
            if (powerMode != PowerMode.NoPower)
            {
                RemovePower();
            }
            RespawnPlayer();
        }
        else
        {
            if (powerMode == PowerMode.NoPower)
            {
                playerHud.ReduceLife();
                if (playerHud.Lives <= 0)
                {
                    Debug.Log("Kill player");
                    Kill();
                    return;
                }

                RespawnPlayer();
            }
            else
            {
                RemovePower();
                untouchable = true;
                StartCoroutine(WaitTillIsTouchableAgain());
            }
        }
    }

    /// <summary>
    /// This function is to prevent remove the power and also reduce one life of the player
    /// Let's wait some time will the player is touchable again to give the player some time to escape
    /// </summary>
    /// <returns></returns>
    IEnumerator WaitTillIsTouchableAgain()
    {
        yield return new WaitForSeconds(0.5f);
        untouchable = false;
    }

    
    /// <summary>
    /// When the player falls or hits an enemy then we remove the power
    /// We switch the colliders to enable the little one again and disable the big one
    /// We finally change the enum powerMode to NoPower and play the sound
    /// </summary>
    private void RemovePower()
    {
        Debug.Log("Remove Mario power");
        playerAnimation.PlayFireToNormalTransitionAnimation();
        m_LittleCollider2D.enabled = true;
        m_BigCollider2D.enabled = false;
        powerMode = PowerMode.NoPower;
        AudioManager.instance.Play("MarioLosesPower");
    }

    /// <summary>
    /// When 
    /// </summary>
    private void RespawnPlayer()
    {
        Debug.Log("Respawn player");
        var closestRespawnPoint = RespawnManager.instance.GetClosestRespawnPoint(transform.position);
        playerMovement.RespawnPlayer(closestRespawnPoint);
        playerAnimation.PlayInjuredAnimation();
    }

    public bool CanShot()
    {
        return powerMode == PowerMode.Fire;
    }


}

