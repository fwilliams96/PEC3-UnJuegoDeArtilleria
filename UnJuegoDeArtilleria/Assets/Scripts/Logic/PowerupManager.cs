using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PowerupManager : MonoBehaviour
{
    private void OnCollisionEnter2D(Collision2D other)
    {
        if (other.gameObject.CompareTag("Player"))
        {
            PickUpPower(other.gameObject);
        }
    }
    
    private void Start()
    {
        InitializePowerup();
        OnPowerupAppears();
    }

    private void Update()
    {
        UpdatePowerup();
    }

    private void FixedUpdate()
    {
        FixedUpdatePowerup();
    }

    protected virtual void InitializePowerup(){}
    protected virtual void UpdatePowerup(){}
    
    protected virtual void FixedUpdatePowerup(){}

    protected virtual void PickUpPower(GameObject player){}

    protected virtual void OnPowerupAppears() {}

}
