using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FireballManager : MonoBehaviour
{
    [SerializeField] private Rigidbody2D m_Rigidbody2D;

    [SerializeField] private int power = 5;
    [SerializeField] private float durationTime = 2; //seconds
    
    private bool counting;
    public GameObject player;

    /// <summary>
    /// This function will be called from the player animation, depending on the player forward the fireball will go to the right or to the left
    /// </summary>
    /// <param name="rightDirection"></param>
    public void ThrowBall(bool rightDirection)
    {
        var horizontalDirection = Vector2.left;
        
        if (rightDirection)
        {
            horizontalDirection = Vector2.right;
        }
        m_Rigidbody2D.AddForce(horizontalDirection * power + Vector2.down * power, ForceMode2D.Impulse);
        counting = true;
    }

    
    /// <summary>
    /// When the ball hits an enemy it will be immediately killed
    /// </summary>
    /// <param name="other"></param>
    private void OnCollisionEnter2D(Collision2D other)
    {
        if (other.gameObject.CompareTag("Enemy"))
        {
            other.gameObject.GetComponent<EnemyManager>().Kill(player);
            Destroy(gameObject);
        }
    }

    /// <summary>
    /// The ball will only have a few time (3 seconds right now) before it disappears
    /// </summary>
    private void Update()
    {
        if (counting)
        {
            durationTime -= Time.deltaTime;
            if ( durationTime <= 0)
            {
                Destroy(gameObject);
            }
        }
        
    }
}
