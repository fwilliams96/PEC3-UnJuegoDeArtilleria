using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Random = UnityEngine.Random;

public class MisteryBoxManager : BasicBoxManager
{

    [SerializeField] private List<GameObject> objects;
    
    /// <summary>
    /// Override the extra function of basic box to disable the box and show a random object from the list of objects
    /// </summary>
    protected override void AddExtraFunctionalityOnCollide()
    {
        DisableBox();
        int randomObjectIndex = Random.Range(0, objects.Count);
        var objectPosition = transform.position + Vector3.up;
        Instantiate(objects[randomObjectIndex], objectPosition, Quaternion.identity);
    }

    /// <summary>
    /// We disable the box by changing the bool canInteract and also triggering the disable state
    /// </summary>
    private void DisableBox()
    {
        canInteract = false;
        m_Animator.SetTrigger("Disable");
    }
}
