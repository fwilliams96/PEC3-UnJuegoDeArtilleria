using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Random = UnityEngine.Random;

public class EnemyMovement : MonoBehaviour
{
    private Vector2 horizontalDirection;
    [SerializeField] private EnemyAnimation enemyAnimation;
    [SerializeField] private Transform frontCheck;
    [SerializeField] private Transform groundCheck;
    [SerializeField] private float horizontalCheckRadius = 0.5f;
    [SerializeField] private float groundCheckRadius = 0.5f;
    [SerializeField] private float speed = 2f;
    private bool isGrounded;
    
    /// <summary>
    /// We draw a gizmos of the circle that is checking the front of the powerup
    /// </summary>
    private void OnDrawGizmos()
    {
        Gizmos.color = Color.yellow;
        Gizmos.DrawSphere(frontCheck.position, horizontalCheckRadius);
        Gizmos.DrawSphere(groundCheck.position, groundCheckRadius);
    }
    
    /// <summary>
    /// When the goomba appears, the horizontal direction will be decided randomly
    /// </summary>
    void Start()
    {
        int randomHorizontalDirection = Random.Range(0, 2);
        if (randomHorizontalDirection == 0)
        {
            FlipHorizontalChecker(); //by default is in the right side
            horizontalDirection = Vector2.left;
        }
        else
        {
            horizontalDirection = Vector2.right;
        }
        
    }

    /// <summary>
    /// The horizontal check will detect if the enemy will collide with some item or brick, in this case will flip horizontally
    /// The goomba will only move if it is on the ground
    /// </summary>
    void Update()
    {
        var finalMaskGroundCheck = (1 << LayerMask.NameToLayer("Platforms")) | (1 << LayerMask.NameToLayer("Items"));

        var horizontalHit = Physics2D.OverlapCircle(frontCheck.position, horizontalCheckRadius, 1 << LayerMask.NameToLayer("Items"));
        isGrounded = Physics2D.OverlapCircle(groundCheck.position, groundCheckRadius, finalMaskGroundCheck);

        if (horizontalHit)
        {
            Debug.Log("Enemy collision with item");

            FlipHorizontalChecker();
            horizontalDirection = horizontalDirection * (-1);
        }
    }

    private void FlipHorizontalChecker()
    {
        // Rotate front check
        var frontCheckLocalPosition = frontCheck.localPosition;
        frontCheckLocalPosition.x = frontCheckLocalPosition.x * (-1);
        frontCheck.position = transform.TransformPoint(frontCheckLocalPosition);
    }

    private void FixedUpdate()
    {
        if (isGrounded)
        {
            transform.Translate(horizontalDirection * Time.deltaTime * speed);
            enemyAnimation.PlayDust();
        }
    }
}
