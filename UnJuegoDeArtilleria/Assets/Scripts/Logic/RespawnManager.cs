using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Numerics;
using UnityEngine;
using Vector3 = UnityEngine.Vector3;

public class RespawnManager : MonoBehaviour
{
    public static RespawnManager instance = null;
    
    [SerializeField] private List<Transform> respawnPoints;
    
    private void Awake()
    {
        instance = this;
    }
    
    void Start()
    {
        SortRespawnPoints();
    }

    /// <summary>
    /// We sort the different respawn points by X position
    /// </summary>
    private void SortRespawnPoints()
    {
        var sortedList = respawnPoints.OrderBy(o=>o.position.x).ToList();
        respawnPoints = sortedList;
    }

    /// <summary>
    /// Using the euclidean distance we calculate the closest respawn point depending on the player position
    /// </summary>
    /// <param name="playerPosition"></param>
    /// <returns></returns>
    public Vector3 GetClosestRespawnPoint(Vector3 playerPosition)
    {
        float minDist = float.MaxValue;
        var closestRespawnPoint = Vector3.zero;
        respawnPoints.ForEach(respawnPoint =>
        {
            var dist = Vector3.Distance(playerPosition, respawnPoint.position);
            if (minDist > dist)
            {
                minDist = dist;
                closestRespawnPoint = respawnPoint.position;
            }
        });
        return closestRespawnPoint;
    }
}
