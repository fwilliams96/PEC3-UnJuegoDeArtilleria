using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyAnimation : MonoBehaviour
{
    [SerializeField] private Animator animator;
    [SerializeField] private ParticleSystem _dust;
    
    /// <summary>
    /// Activate death animation of the enemy
    /// </summary>
    public void PlayDeathAnimation()
    {
        animator.SetTrigger("Death");
    }

    /// <summary>
    /// Play dust of the enemy
    /// </summary>
    public void PlayDust()
    {
        if (_dust != null)
        {
            _dust.Play();    
        }
    }
}
