# Introducción
Bienvenidos al repositorio de la replicación mejorada realizada del videojuego de plataformas tan conocido como Super Mario Bros.

# Descripción del videojuego y cómo jugar
Este videojuego es la versión mejorada de la práctica anterior que se centraba únicamente en las plataformas. En dicha versión no existían powerups para cambiar el estado de mario ni la posibilidad de disparar a los enemigos para matarlos.
Los powerups aparecerán en las cajas misteriosas de forma random, nos puede tocar un powerup o bien nos puede tocar una moneda.
Se ha introducido otro cambio en esta versión relativo a las vidas, en este caso tendremos 3 vidas que se nos mostrarán arriba junto a las monedas y que una vez agotadas se reiniciará el nivel.
Las teclas que usaremos para manejar el jugador serán A y D para las direcciones, espacio para salto y el click izquierdo para el disparo (estando en la forma de fuego de Mario).
Los enemigos que nos encontraremos serán los Goombas, que podremos matar saltando encima o bien con nuestra bola de fuego. Esto sumará puntos a nuestra partida.
El resto de mecánicas serán iguales que las de la versión anterior, el objetivo final consiste en llegar a la meta o goal point antes de que se acabe el tiempo, o ahora en este caso las 3 vidas que tendremos.

# Implementación del videojuego
## Funcionalidades implementadas
Se han logrado implementar todos los puntos requeridos, algunos de una forma más básica y otros de una forma más consistente. En esta práctica no existen puntos opcionales por lo que se ha optado por intentar pulir elementos que pudieran dañar la jugabilidad. 
Al final del documento se comentarán las dificultades más destacadas que se han encontrado.

## Estructura
La organización de los scripts se ha mantenido tal cual como en la práctica anterior, como modelo-vista-controlador, de las cuales explicaremos una a una los scripts involucrados:

### Capa modelo 
Se mantiene el script **Sound** para facilitar la creación de sonidos desde el inspector del manager de sonidos que comentaremos a continuación.
Además, se ha añadido el script **GeneralData** (como Singleton) para guardar todo los datos del videojuego que fueran globales. En este caso hemos guardado el prefab de las bolas de fuego. De esta forma facilitamos el acceso a todo el resto de scripts.

### Capa lógica
En este videojuego, hay que destacar que se deben controlar los siguientes elementos:
- Lógica del videojuego en sí
- Personaje
- Enemigos
- Cajas
- Monedas
- Bola de fuego
- Resto elementos que pertenecen al videojuego (tiempo límite, goal point, etc)

Empezando por la lógica central del videojuego, se concentra en el script **GameManager**, este script es bastante sencillo y básicamente se encarga de manejar los siguientes estados:
- MainMenu: Cuando apenas se abre el videojuego pero todavía no se mueve el jugador ni se está contando el tiempo.
- StartGame: Cuando el jugador ha apretado alguna tecla, transicionamos desde el estado anterior a este estado. Aquí ya podemos ver como movemos al personaje y el tiempo empieza a descontarse.
- Playing: El estado que tiene el videojuego mientras se está jugando, no tiene ninguna acción asociada (en los FSM podría contemplarse como un estado nulo o vacío).
- GameOver: Aquí se muestra la pantalla de GameOver al jugador y un botón para volver a empezar.
- Victory: Aquí en cambio mostramos la pantalla de victoria al jugador con un botón para volver a jugar también (en un videojuego con más niveles pasaríamos al siguiente nivel).
- EndGame: Sería otro estado "vacío" en el que nos encontraríamos hasta que el jugador pulsase el botón "Try again" o "Try again" de las pantallas de gameover o victory respectivamente.

Por lo que nos encontraremos que esta capa tiene gran peso respecto las otras, para comentar los scripts involucrados empezamos por los que tienen que ver con el personaje. 
Para el personaje hay que tener en cuenta tanto el movimiento como la lógica propia del personaje. El movimiento se ha separado en 3 scripts distintos
- **PlayerInput**: Primer script del movimiento del jugador que únicamente se encarga de leer las teclas del jugador y determinar si su intención es moverse horizontalmente, saltar o disparar.
- **PlayerMovement**: Es el segundo script de la cadena de movimiento que comprueba las intenciones del jugador y ejecuta la acción. Cuando éste quiere moverse horizontalmente, desplaza el rigibody con una velocidad parametrizable. Si por lo contrario desea saltar, aplica la fuerza en el eje de la Y con una fuerza de salto también parametrizable. Con la nueva funcionalidad de disparar, comprueba con el script que almacena la lógica del jugador **PlayerManager** si el personaje puede disparar, en ese caso invoca al **PlayerAnimation** para realizar dicha acción (y todas las demás acciones).
- **PlayerAnimation**: Es el tercer script de la cadena de movimiento, este script es el que tiene acceso directo al componente Animator, el SpriteRenderer y da comienzo o para las mismas. Además se encarga de ejecutar los sonidos que sean invocados por las mismas animaciones. Es invocado tanto por el script **PlayerMovement** como el **PlayerManager**.
- **PlayerManager**: Se encarga de la lógica propia del personaje, orientado a los cambios que pueda sufrir el personaje por fuerzas externas al jugador. En este caso cuando choca con algún enemigo y debe perder una vida o el poder actual, cuando gana, cuando se le añaden puntos o cuando se le añaden monedas. Por dentro se comunica con el script **GameManager** para cambiar los estados de la partida.

Para manejar la lógica del enemigo patrullero, en este caso los Goombas, usamos 2 scripts distintos:
- **Waypoint Patrol**: Su objetivo es el de patrullar las zonas que nosotros le pasemos como entrada en el inspector (se tratan solo de GameObject vacíos de los cuales se obtiene el Transform), puede realizar una patrulla de más de 2 posiciones, pero en este videojuego solo hemos usado 2. Para controlar la velocidad de desplazamiento usamos el parámetro moveSpeed de la entrada.
- **EnemyManager**: Contiene la lógica propia del enemigo cuando éste interactúa con el jugador. Cuando el jugador colisiona con él, la función OnColliderEnter2D se encarga de determinar si la colisión ha sido encima o debajo de la altura Y de un parámetro de entrada llamado deathPoint. Podría haberse hecho con un collider aparte pero no se ha considerado un impacto en el videojuego. En caso de que el jugador choque por encima de esta altura el enemigo ejecuta la animación de muerte y al poco tiempo se elimina de la escena.
En caso que el jugador choque por debajo se llama al método ReduceLife() del script **PlayerManager**, donde el jugador acabará perdiendo una vida de las 3 posibles o podría no perder ninguna vida si en ese momento se encuentra en el estado de fuego (pero sí perdería el poder).
Este script también actuará cuando una bola de fuego del jugador choque contra el mismo, procediendo de la misma forma que si el jugador cayera encima del enemigo.
- **EnemyAnimation**: A diferencia de la práctica anterior, se ha decidido aislar lo que son animaciones propias del enemigo. Aquí nos encontraremos tanto el dust aplicado al goomba como la interacción con el componente Animator.

Para los goombas con un mínimo de inteligencia, hemos añadido el script **EnemyMovement** para mantener al enemigo en constante movimiento y voltearlo horizontalmente cuando choque con algún elemento. Más abajo explicamos las mejoras respecto este script.

Dentro de las cajas incluidas en la partida, tenemos dos scripts distintos, uno para cada una:
- **BasicBoxManager**: Se encarga únicamente de gestionar la colisión de entrada y salida contra el jugador. En ese caso empieza o termina la animación de agitar la caja. Pero dado a que la siguiente caja, la misteriosa, comparte este comportamiento, se pensó que sería interesante declarar unos métodos virtuales que luego la subclase de la caja misteriosa sobreescribiera para añadirle más funcionalidades a la caja básica.
- **MisteryBoxManager**: Como se acaba de comentar, este script hereda de **BasicBoxManager**, ya que comparte el comportamiento de agitamiento. Pero además, se ha sobreescrito los métodos declarados en la super clase para permitir sacar un elemento de forma random de los posibles, y de deshabilitarla. Para ello se creó el método _DisableBox()_ que es invocado en el método _AddExtraFunctionalityOnExit()_. Además se instancia el GameObject del objeto random que se vaya a mostrar (ya no existe un elemento pre-instanciado como en la práctica anterior).

Para la moneda, existe también un script asociado, llamado **CoinManager** pero lo único que hace es notificar al jugador de que se ha cogido una moneda (mediante colisión) y luego acabarse destruyendo.

Finalmente, tenemos otros scripts que se encargan de elementos externos:
- **DeadZone**: Script asociado a un collider por debajo de las plataformas que identifica si el jugador ha caído al vacío para notificar al **GameManager** de que el jugador debe perder una vida y así finalizar la partida.
- **WinZone**: Script asociado a un collider al final del nivel o goal point que notifica al **GameManager** que el jugador ha ganado la partida.
- **GameTimer**: Script asociado al temporizador que tenemos en el HUD del jugador y que nos actualiza los segundos restantes para superar el nivel, en caso de agotarse notifica al **GameManager** la derrota inmediata del jugador.
- **AudioManager**: Script asociado a la reproducción de los distintos sonidos del videojuego. A partir del **Sound** MainTheme y la lista de **Sound** llamada Effects se encarga de instanciar dos AudioSource, uno para el sonido de fondo y otro para todos los efectos de sonido restantes que podrá ser invocado mediante el mismo script con la función _Play()_ y el nombre del sonido a ejecutar. Permite modificar el volumen y el pitch asociado a cada sonido que arrastremos.
- **EnemyRespawner**: Script asociado a la instanciación de nuevos Goombas en puntos aleatorios del nivel con un tiempo parametrizable en el editor. En este caso instanciaría los goombas que se mueven con el script **EnemyMovement**.

### Capa visual
Dentro de esta capa solo tenemos 3 scripts:
- **LockCameraY**: Este script es la solución a que la cámara cinemachine no siga la posición Y del jugador, así manteniendose siempre a la misma altura (la camára del videojuego original funciona así). Solamente hay que añadirlo como extensión a la virtual camera.
- **DamageEffect**: Este script se encarga de mostrar el texto correspondiente al daño realizado al Goomba cuando este es eliminado por el protagonista. Recibe el componente del texto correspondiente y un canvas group para hacer que la desaparición del mensaje sea de forma suave usando el alpha.
- **PlayerHUD**: Este script se encuentra asociado al canvas de la información de la partida y permite actualizar los 4 elementos del HUD: puntuación, monedas recogidas, vidas restantes del jugador y tiempo restante (a diferencia de la práctica anterior ahora se ha añadido el elemento de vidas).

## Físicas y colisiones
### Componentes de física y colisiones del jugador
![Animaciones de Mario](./Docs/ColliderAndPhysicalComponents.PNG)

En la imagen podemos ver los componentes Rigidbody2D y CapsuleCollider2D utilizados y con sus parámetros correspondientes.
Para empezar, el rigibody tiene el body type como "Dynamic" para que las fuerzas le afecten y tener una masa finita. Se le ha añadido un escala de gravedad de 8 puntos a medida que se iban haciendo las pruebas ya que se ha considerado como la más adecuada teniendo en cuenta el funcionamiento del videojuego original (las caídas son bastante rápidas).
Finalmente se le ha aplicado una pequeña fricción de 0.1 para evitar cualquier problema de desnivel en las plataformas que hiciera que el personaje con el material teflón pudiera mantenerse moviendo solo.
Como tipo de detección para las colisiones se ha usado "Continous" para evitar que por cualquier retraso en las comprobaciones el personaje pudiera traspasar alguna plataforma u otro elemento. Si algún momento pasara que el jugador traspase algún elemento antes de que el evento de colisión se active, esta opción lo detectaría y arreglaría.
El tipo de interpolación se ha usado "Interpolate" para que el movimiento se muestre de forma suavizada usando las posiciones del jugador en frames anteriores.

Para el componente CapsuleCollider2D, se ha añadido un material de tipo Teflon para evitar fricción en el personaje, esto era necesario para evitar que se quedara pegado en las plataformas o cajas al saltar contra ellas.

### Componentes de física de las bolas de fuego y los mushrooms
Al añadir los mushrooms, hubo que hacer ciertos ajustes respecto la fuerza con la que caía de las cajas de arriba para que fuera lo más coherente posible al sistema de físicas de Mario Bros. Por ello le añadimos un gravity scale de 4.
Lo mismo sucedió con las bolas de fuego, son bastante parecidas a las del juego original pero era muy difícil conseguir que cayeran al vacío estando el gravity scale del rigidbody a 1, por lo que 4 fue un buen valor para ellas también.


### Tipos de colisiones llevadas a cabo en la partida

- Colisiones mediante los eventos propios de Unity: Para la detección de colisiones con el enemigo o las monedas se usan las colisiones normales. Pero para la zona de victoria o la zona de vacío se usan triggers, de esta forma evitamos que el jugador se choque contra estas zonas realmente no visibles.
- Colisión del jugador con el suelo: A la hora de detectar si el jugador se encuentra en el suelo o no, se ha optado por usar la solución de raycasts usando el método _Physics2D.OverlapCircle_, ya que así evitamos sobreecargar los eventos de colisión del jugador y de tener que ajustar perfectamente el collider.
- Comprobación del tipo de colisión con el enemigo: Cuando el enemigo choca con el jugador (por el evento propio de Unity), dependiendo de donde hemos chocado con el enemigo, morirá o nos eliminará. Aquí se ha optado por usar una posición local del Goomba de la cual tendremos en cuenta la posición Y para determinar si el jugador ha chocado por encima de esta altura mínima (y así eliminar al Goomba) o por si lo contrario ha chocado por debajo y será el jugador el que perderá.
- Colisión con ambos tipos de cajas: Este tipo de colisión al principio parecía trivial pero finalmente con las pruebas se ha visto que realmente el jugador solo puede activarlas si choca con ellas desde abajo. Por lo que para este problema sí que se ha optado por usar dos colliders, uno para envolver la caja y otro bastante más pequeño que cubra básicamente solo la parte más baja de la caja. De esta forma solo se activa el evento cuando choca con este segundo collider. Aquí también podría haberse hecho uso de una altura máxima pero finalmente se ha optado por la solución del segundo collider ya que la forma es cuadrada a diferencia del Goomba.
- Colisión de la bola de fuego con los enemigos: Esta colisión es solamente de las bolas de fuego con los GameObject que tengan el tag de "Enemy", en este caso se procede a invocar al método _Kill()_ del script **EnemyManager**. Sea el enemigo que sea (en caso de crear nuevos tipos de enemigos).
- Colisiones para los distintos estados de Mario: Se ha añadido un nuevo collider en el personaje para cuando éste se encuentre en el estado de fuego. Entonces al cambiar de estado se desactiva uno de los colliders y se activa el otro (un collider pequeño y el otro grande).

A diferencia de la práctica anterior, se crearon 4 nuevas layers, esto fue para facilitar la colisión entre los distintos elementos y también ahorrar cálculos de colisiones que no nos interesan. Por ejemplo:
- No se quieren colisiones entre las mismas fireball
Esto provoca que si se lanzan sobre un mismo punto, al tubo verde por ejemplo, se acumulen en el mismo punto ocupando más espacio del necesario. Al quitar la colisión entre ellas mismas se consiguió que no sucediera esto. En las imagenes de debajo se pueden ver las diferencias de lo que se comenta:
![Bolas de fuego con colisión en la misma capa](./Docs/FireballGroup.PNG)
![Bolas de fuego sin colisión en la misma capa](./Docs/FireballGroup2.PNG)

- No se quieren colisiones entre los enemigos y los powerups que se estén desplazando: De esta forma permitimos que los powerups "atraviesen" a los enemigos sin problema y no tengan que cambiar de dirección o mantenerse constantemente chocando contra ellos (sin poder pasar)

- No se quieren colisiones entre los mismos powerups: Al igual que con las fireballs, no tiene sentido que colisionen entre ellas mismas si una powerup va a una dirección y la otra a la contraria. Así evitamos estos choques "infinitos"

- Colisiones entre enemigos: Tampoco queremos colisiones entre los mismos enemigos, por el mismo motivo comentado en los casos anteriores.

- Colisiones entre fireballs y powerups: Tampoco nos interesan colisiones entre los mushrooms y las bolas de fuego. Preferiblemente mejor que las bolas de fuego atraviesen estos powerups.

- Colisiones entre el player y fireballs: Finalmente, no queremos colisiones entre player y fireballs ya que parecen algo extrañas (a veces se te puede quedar una  bola de fuego en la cabeza). Aunque tampoco es un detalle importante puesto que las bolas de fuego desaparecen a los 3 segundos.

En la siguiente imagen podemos ver como ha quedado finalmente nuestra matriz de colisiones:
![Matriz de colisiones](./Docs/LayerCollisionMatrix.PNG)

## Aspectos visuales y sensoriales
Dentro de los aspectos visuales, explicaremos brevemente las diferentes animaciones que se han incluido en el videojuego:

**Animaciones del protagonista Mario (gran cambio sufrido respecto la práctica anterior)**
![Animaciones de Mario](./Docs/MarioController.PNG)
Aquí podemos destacar que tiene 7 estados distintos: _Idle_, _Run_, _Jump_, _Death_, _PowerTransition_, _NormalTransition_ y _Shot_. 
Antes de comentar brevemente cada estado, comentamos la solución adoptada para permitir a Mario tener solo estas animaciones que cubren tanto el estado de Mario normal como el estado de Mario de fuego.
Al investigar como la mayoría de soluciones de Internet apostaban por crear nuevos estados por cada nuevo tipo de Mario que existiese: _FireIdle_, _IceIdle_ y así consecutivamente. Me vi en la obligación de buscar más profundamente para encontrar una solución más escalable, ya que editar el mismo diagrama por cada versión que existiese de Mario sería completamente ineficiente e induciría a muchos errores al querer sincronizar los estados.
Finalmente, di con la opción de crear una nueva _Layer_ de tipo sync en el controlador de Mario, esto se usa mucho para cuando se quiere mantener el mismo diagrama pero con distintos clips de animación (usado para las animaciones del jugador malherido). Y esto básicamente es lo que necesitaba. Mario al cambiar al estado de fuego mantiene exactamente las mismas animaciones que la versión normal, a diferencia de que se añade la animación de disparo.
Por ello mantenemos el mismo diagrama base pero añadiendo otros 3 estados: 2 para la transición de Mario normal a Mario mejorado y al revés y 1 para el disparo.
Cabe comentar, que para cambiar de una layer a otra es necesario cambiar el weight de la capa. Para ello hemos usado la función _SetLayerWeight()_ del Animator.

Dicho esto, explicamos brevemente cada estado con la nueva solución:
- Idle: Aquí modificamos 2 propiedades distintas: el sprite y la escala. En el sprite tenemos asociado únicamente uno pero sí que se ha jugado con la escala para dar una sensación de que el personaje respira a lo largo de 40 fotogramas.
- Run: Para activarse se usa el booleano "IsRunning". Únicamente se juega con la propiedad sprite a lo largo de 10 fotogramas. Este valor se ha ajustado bastante al movimiento del videojuego original.
- Jump: Se activa con el trigger "Jump" desde cualquier estado. Dentro se usa únicamente un sprite pero a lo largo de 30 fotogramas para así forzar a la que la animación dure lo suficiente para visualizarla, ya que para volver a los estados de _Idle_ o _Run_ tiene el check de "Exit Time" marcado. Esta animación se activa mediante trigger desde cualquier estado.
- Death: Se activa con el trigger "Death" desde cualquier estado y juega únicamente con un sprite que es el que muestra a Mario con las manos en la cabeza y de frente a la pantalla.
- Power transition: Este estado se activa con el trigger "Power", en este caso mostramos la transición de Mario pequeño a Mario grande de fuego.
- Normal transition: La misma transición que la anterior pero en este caso al revés, de Mario de fuego a Mario pequeño. Activada por el trigger "Normal".
- Shot: Esta animación, lanzada por el trigger "Shot", es la invocada cuando Mario de fuego dispara, por lo que solo tiene sentido cuando Mario es de fuego.

**Animaciones del enemigo Goomba**

![Animaciones de Goomba](./Docs/GoombaController.PNG)

En este controlador encontramos únicamente dos animaciones: _Walk_ y _Death_. 
- La primera animación es la que se encuentra por defecto y es la que usa 2 sprites del Goomba para dar la sensación que camina a lo largo de 20 fotogramas.
- La otra animación de muerte, es para mostrar al Goomba chafado por Mario después de que este lo pise. Esta última se activa con el trigger "Death".

**Animaciones de la moneda**

![Animaciones de la moneda](./Docs/CoinController.PNG)

En este controlador solo tenemos una animación y es la que juega con 2 sprites distintos a lo largo de 20 fotogramas para darle un poco de vivez a la moneda.
Este controlador no usa ningún parámetro ya existe únicamente una sola animación.

**Animaciones del bloque ladrillo**

![Animaciones del bloque ladrillo](./Docs/BrickBoxController.PNG)

En este controlador podemos observar dos animaciones, la de _Quiet_ y _Shake_. La primera es el estado por defecto que no realiza ninguna animación y la segunda es la que se ejecuta cuando es golpeada por el personaje. Se activa mediante el trigger "Shake".

**Animaciones del bloque misterioso**

![Animaciones del bloque ladrillo](./Docs/MisteryBoxController.PNG)

El controlador contiene la misma estructura que el anterior controlador del ladrillo pero añade un nuevo estado, el _Disabled_ (activado mediante el trigger "Disable"). Este estado será activado cuando el jugador golpee la caja para hacer aparecer la moneda de su interior. Una vez activado este estado se mantendrá así el resto de la partida.

Dentro de los aspectos sensoriales, destacamos el uso del script **AudioManager** que permite tener centralizada la parte de los efectos de sonido y el sonido de fondo. Estos sonidos han sido tanto invocados desde código y también algunas veces invocados puntualmente desde las animaciones como "Animation Event". 

Como último punto, tenemos la introducción de sistema de partículas, para ello se ha creado un GameObject vacío tanto dentro del jugador como de los goombas. En sí el sistema de partículas es el mismo para ambos personajes. En el caso de los goombas es constantemente llamado cuando este se encuentra en movimiento (que es básicamente siempre exceptuando cuando está cayendo) y para Mario cuando éste cambia de dirección o da un salto.
Este sistema de partículas es manejado desde los scripts de animación de los distintos personajes, para así mantener todo de la forma más centralizada posible.

## Mejoras de los enemigos
En la versión anterior de la práctica habíamos comentado que únicamente existían los Goomba patrulleros, estos se mueven de un punto a otro de forma indiferente, no tienen ninguna lógica por sí mismos.
A raíz de haber hecho que los mushrooms se movieran y se voltearan al chocar con elementos, no se pudo evitar la tentación de realizar esto también para los Goombas. Por lo que acabamos implementando el mismo funcionamiento para ellos.
Pero nos faltaba un sistema de respawn, para así acabar incrementando la dificultad del nivel. Entonces se creó el script **EnemyRespawner** que cada 2 segundos instancia un nuevo goomba de los que modifica su dirección al chocar contra algo. Al instanciarse se hace un random para ir a la derecha o izquierda, así dando un poco más de realismo.
El script encargado de mover al enemigo se trataría del **EnemyMovement**, el cual básicamente haría uso de los Transform frontCheck y groundCheck para desplazar al enemigo.
El transform frontCheck serviría para detectar cuando se choca con un objeto (usando Raycast) y así voltear el enemigo. En cambio groundCheck serviría para detectar, mediante raycast también, cuando el enemigo está en el suelo. De esta forma evitamos desplazar al enemigo horizontalmente cuando está cayendo verticalmente (un poco más de realismo).
Finalmente, a nivel de enemigos mantenemos tanto los patrulleros como los que sí tienen una mínima inteligencia (y que se irían instanciando de forma aleatoria por distintos puntos del nivel).

![Respawn de Goombas](./Docs/EnemyRespawner.PNG)

## Respawn del jugador
A diferencia de la práctica anterior, no teníamos un sistema de vidas ni tampoco un sistema de respawn del personaje. Aquí hemos incluido la posibilidad de respawn cuando al jugador se le arrebata una de las vidas.
Para ello hemos usado el script RespawnManager que tiene un array de distintos respawns (posiciones) y de las cuales nos interesará saber la más cercana para así permitir al personaje reaparecer. Para ello se ha usado el cálculo de la distancia euclidiana.

![Respawn del jugador](./Docs/RespawnPlayer.PNG)

## Dificultades encontradas
Han existido varias dificultades en la mejora del videojuego respecto la versión anterior, pero podemos destacar las siguientes:

### Las animaciones de transición de Mario pequeño a Mario de fuego (y la opuesta)
Quería ser fiel a la transición que vi en algún gameplay (no sé si es la original realmente), cuando Mario cambia de pequeño a grande sucede que Mario pequeño por unos momentos "flota" antes de convertirse en Mario grande. Esto se puede ver demostrado en la imagen de abajo, el problema que existía en este caso es que todos los componentes de Animator y Sprite Renderer se encontraban en el GameObject más externo del Prefab y esto daba problemas si se quería desplazar a Mario por el eje Y para ser fiel a esta versión. El problema es que si se mueve la Y del jugador en el Animator (tal y como lo tenía antes), lo estaba desplazando en coordenadas reales del mundo, cuando en realidad quería moverlo en coordenadas locales respecto las mismas coordenadas globales en las que se encontrara el jugador.
Para lograr esto lo más fácil fue mover los componentes Animator y SpriteRenderer a un GameObject interno llamado Body. De esta forma al animar y mover la coordenada Y de Mario lo estaría haciendo con las coordenadas locales del GameObject y así no tendría dicho problema.
Este cambio además ahorra muchos problemas a futuro respecto este tipo de animaciones.
![Transición a estado de fuego](./Docs/FireTransition.PNG)

### Añadir el pestañeo rojo del personaje cuando este es dañado
Diría que en la versión de 1985 no existe un sistema de vidas en Mario, por lo que este detalle realmente no existe, pero dado que uno de los puntos era tener un sistema de respawn, decidí darle a Mario 3 vidas.
La cuestión es que cuando Mario se cae, choca contra un Goomba (sin poder), veía necesario darle un pequeño feedback al jugador de esto (aparte del propio sonido). Así que lo primero que hice fue crear una animación pero no me acababa de convencer ni veía necesario crear una animación aparte para solamente añadir este pequeño efecto (además de que me estaba dando problemas a la hora de hacer las transiciones). Por lo que finalmente me decanté por cambiarle el color directamente al sprite (independientemente del sprite que tuviera Mario en ese momento). Esto se hizo mediante un bucle que le cambiaba el color n veces (para no ser muy larga n se quedó en 4). Entonces mediante el uso de una coroutine, Mario estaría rojo 0.2 segundos y luego del color normal otros 0.2 segundos. Dando así como resultado el pestañeo de colores típico llamado flash o blink.

### Enemigo a veces quitaba 2 vidas de golpe
Sucedía que cuando chocaba con el Goomba, este a veces me quitaba 2 vidas de inmediato, esto es lógico ya que puede entrar en el colisionador 2 veces. Pero da una sensación extraña al jugador ya que no sería fiel a la experiencia que dan la mayoría de juegos. En estos normalmente te dan una especie de "inmunidad" cuando te quitan una vida, durante un cierto tiempo, hasta que el enemigo puede volver a quitarte otra vida. Para esto usé el booleano que usaba para no matar dos veces al enemigo (que sucedía en la versión anterior) y lo usé para que en un transcurso de 1 segundo, el jugador no pudiera ser atacado de nuevo por los enemigos. Con esto evitamos tener estos casos extraños que no sabemos porque pasan pero tienen todo el sentido del mundo.

### Pestañeo del Mario malherido dejaba al Mario en rojo de forma permanente
Este es un problema menor pero también curioso de comentar, la función que cambia el color a rojo para el efecto de malherido, antes de poner el color rojizo se guardaba el color original en una variable antes del bucle de pestañeos. El problema es que si me suicidaba dos veces seguidas el color original al entrar en la misma función por segunda vez sería el rojo (al estarse ejecutando dos veces la misma función), así dando a lugar que el jugador quedara permanentemente rojo.
Para arreglarlo simplemente recojo el color original en el _Start()_ del script y posteriormente lo aplico en la función para así volver a dejar el personaje en su color original.

### Movimiento mushroom (powerups)
Se quería conseguir que los mushrooms se movieran apenas salir de las cajas misteriosas, el problema fue en que hubo que cambiar gran parte del diseño de los items y plataformas para conseguir esto. Ya que cuando el mushroom choca contra contra el tubo o algún bloque horizontalmente, éste debe cambiar la trayectoria horizontal para "voltear". 
El problema era que algunos bloques horizontales se encontraban como layer "Platforms" en vez de "Items" y esto provocaba que se quedara el mushroom pegado al bloque sin voltearse. Para ello se movieron esos bloques de capa con el mismo Tile Palette y finalmente se les asignó el layer "Items" correspondiente.

# Presentación del videojuego
- [Demostración del videojuego](https://youtu.be/A990vn5TC68)

# Publicación
- WebGL: [Un juego de plataformas para WebGL](https://informaticoloco-33.itch.io/unjuegodeartilleria)

# Licencias / Recursos utilizados
- Sonidos: [Themushroomkingdom](https://themushroomkingdom.net/) (solicitar permiso en caso de usar el contenido en un sitio web propio).
- Fuente de letra: [Fontstruct](https://fontstruct.com/) (bajo licencia de SIL Open Font License para ser usada pero sin revenderla).
- Imágenes: [Spriters Resource](https://www.spriters-resource.com/) (en este caso al ser sprites originales del videojuego no son aptos para uso comercial ).
